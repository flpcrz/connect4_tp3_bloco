# Keith Galli (https://github.com/KeithGalli/Connect4-Python)
# How to Program Connect 4 in Python! (part 1) - Basic Structure & Game Loop (https://www.youtube.com/watch?v=UYgyRArKDEs)
# How to Program Connect 4 in Python! (part 2) - Check for Winning Move (https://www.youtube.com/watch?v=zD-Xuu_Jpe4)
# ...

import numpy as np

ROW_COUNT = 6
COLUMN_COUNT = 7
WINNER_CRITERIA = 5

def create_board():
	board = np.zeros((ROW_COUNT,COLUMN_COUNT))
	return board

def drop_piece(board, row, col, piece):
	board[row][col] = piece

def is_valid_location(board, col):
	return board[ROW_COUNT-1][col] == 0

def get_next_open_row(board, col):
	for r in range(ROW_COUNT):
		if board[r][col] == 0:
			return r

def print_board(board):
	print(np.flip(board, 0))

def winning_move(board, piece):
	# Check horizontal locations for win
	for c in range(COLUMN_COUNT-(WINNER_CRITERIA - 1)):
		for r in range(ROW_COUNT):
			winnerRow = []
			for w in range(WINNER_CRITERIA):
				if board[r][c+w] == piece:
					winnerRow.append(True)
			if len(winnerRow) == WINNER_CRITERIA:
				return True

	# Check vertical locations for win
	for c in range(COLUMN_COUNT):
		for r in range(ROW_COUNT-(WINNER_CRITERIA - 1)):
			winnerCol = []
			for w in range(WINNER_CRITERIA):
				if board[r+w][c] == piece:
					winnerCol.append(True)
			if len(winnerCol) == WINNER_CRITERIA:
				return True			

	# Check positively sloped diagonals
	for c in range(COLUMN_COUNT-(WINNER_CRITERIA - 1)):
		for r in range(ROW_COUNT-(WINNER_CRITERIA - 1)):
			winnerPositiveDiag = []
			for w in range(WINNER_CRITERIA):
				if board[r+w][c+w] == piece:
					winnerPositiveDiag.append(True)
			if len(winnerPositiveDiag) == WINNER_CRITERIA:
				return True			

	# Check negatively sloped diagonals
	for c in range(COLUMN_COUNT-(WINNER_CRITERIA - 1)):
		for r in range((WINNER_CRITERIA - 1), ROW_COUNT):
			winnerNegativeDiag = []
			for w in range(WINNER_CRITERIA):
				if board[r-w][c+w] == piece:
					winnerNegativeDiag.append(True)
			if len(winnerNegativeDiag) == WINNER_CRITERIA:
				return True				

board = create_board()
print_board(board)
game_over = False
turn = 0

while not game_over:
    # Ask for Player 1 input
    if turn == 0:
        col = int(input("PLAYER 1, make your selection (0-6):"))

        if is_valid_location(board, col):
            row = get_next_open_row(board, col)
            drop_piece(board, row, col, 1)

            if winning_move(board, 1):
                print("PLAYER 1 wins!!!! Congrats!!!")
                game_over = True


    # Ask for Player 2 Input
    else:				
        col = int(input("PLAYER 2, make your selection (0-6):"))

        if is_valid_location(board, col):
            row = get_next_open_row(board, col)
            drop_piece(board, row, col, 2)

            if winning_move(board, 2):
                print("PLAYER 2 wins!!!! Congrats!!!")
                game_over = True

    print_board(board)

    turn += 1
    turn = turn % 2
